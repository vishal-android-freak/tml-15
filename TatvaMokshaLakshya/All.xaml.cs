﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace TatvaMokshaLakshya
{
    public partial class All : PhoneApplicationPage
    {
        public All()
        {
            InitializeComponent();
        }

        string eName, eDescr, ePrice;

        //Catch parameters sent from Tava, Moksh and Lakshya events and display them.
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (NavigationContext.QueryString.TryGetValue("eName", out eName) | NavigationContext.QueryString.TryGetValue("eDescr", out eDescr) |  NavigationContext.QueryString.TryGetValue("ePrice", out ePrice))
            {
                Event_Name.Text = eName;
                Event_Desc.Text = eDescr;
                Price.Text = ePrice;
            }
        }
    }
}