﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using TatvaMokshaLakshya.ViewModels;

namespace TatvaMokshaLakshya
{
    public partial class DescriptionTatva : PhoneApplicationPage
    {
        AccessSocialMedia access = new AccessSocialMedia();
        public DescriptionTatva()
        {
            InitializeComponent();
        }

        //Navigation Handlers for events list, Facebook and YouTube
        private void TatvaEvent_Click(object sender, RoutedEventArgs e)
        {
                NavigationService.Navigate(new Uri("/TatvaEvents.xaml", UriKind.Relative));
        }

        private void FB_Click(object sender, System.Windows.Input.GestureEventArgs e)
        {
            access.onLinkRedirect("https://www.facebook.com/TatvaMokshLakshya");
        }

        private void Youtube_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            access.onLinkRedirect("https://www.youtube.com/channel/UCJ0DfvRtZuEewD4RbLyMndQ");
        }

    }
}