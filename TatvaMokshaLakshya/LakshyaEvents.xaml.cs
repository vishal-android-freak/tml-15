﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace TatvaMokshaLakshya
{
    public partial class LakshyaEvents : PhoneApplicationPage
    {

        long LastID;
        string tml;
        public LakshyaEvents()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var db = App.TMLEvents;

            //Check if data exists on first navigation to this page, in the table. 
            //if not, means there is no internet connection.
            try
            {
                using (var last = db.Prepare("SELECT eid FROM events_table ORDER BY eid DESC"))
                {
                    last.Step();
                    LastID = (Int64)last[0];
                }
            }
            catch (Exception)
            {
                Not.Visibility = System.Windows.Visibility.Visible;

                Not.Text = "Event list needs you to be connected to the internet";
                MessageBox.Show("Please check your internet connection!");
            }



            //Add child elements to the main event list
            List<TMLEvents> list = new List<TMLEvents>();
            int i = 1;


            while (i <= LastID)
            {
                using (var tatva = db.Prepare("SELECT eid, event_name, tml, descr, Price FROM events_table WHERE eid=?"))
                {
                    tatva.Bind(1, i);
                    tatva.Step();

                    using (var version = db.Prepare("SELECT eid, tml FROM events_table WHERE eid=?"))
                    {
                        version.Bind(1, i);
                        version.Step();
                        tml = (string)version[1];
                    }

                    if (tml == "3")
                    {
                        list.Add(new TMLEvents() { Event_Name = (string)tatva[1], Description = (string)tatva[3], Price = (string)tatva[4] });
                        EventsView.ItemsSource = list;
                    }
                    i++;
                }
            }

        }
        public class TMLEvents
        {
            public string Event_Name { get; set; }
            public string TML { get; set; }
            public string Description { get; set; }
            public string Price { get; set; }
        }

        private void Lakshya_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TMLEvents id = EventsView.SelectedItem as TMLEvents;

            var _event_name = id.Event_Name;
            var _descr = id.Description;
            var _price = id.Price;

            NavigationService.Navigate(new Uri("/All.xaml?eName=" + _event_name + "&eDescr=" + _descr + "&ePrice=" + _price, UriKind.Relative));
        }
    }
}