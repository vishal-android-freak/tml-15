﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TatvaMokshaLakshya.ViewModels;
using Newtonsoft.Json;

namespace TatvaMokshaLakshya
{
    public partial class MainPage : PhoneApplicationPage
    {
        public String EID = "eid";
        public String EVENT_NAME = "event_name";
        public String TML = "tml";
        public String DESCR = "descr";
        public String PRICE = "Price";
        public String DAY = "day";
        int count = 0;
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;


        }

        // Load data for the ViewModel Items
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            var db = App.TMLEvents;

            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }

            base.OnNavigatedTo(e);

            //Try for internet connectivity
            try
            {
                string aagayaHai = await RegisterData.GetEventDetails();

                dynamic obj = JsonConvert.DeserializeObject<dynamic>(aagayaHai);


                //Add data into the table which is extracted from JSON array
                foreach (var o in obj)
                {
                    using (var insert = db.Prepare("INSERT INTO events_table(eid, event_name, tml, descr, Price, day) VALUES (?,?,?,?,?,?)"))
                    {
                        EID = (string)o["eid"];
                        insert.Bind(1, EID);
                        EVENT_NAME = (string)o["event_name"];
                        insert.Bind(2, EVENT_NAME);
                        TML = (string)o["tml"];
                        insert.Bind(3, TML);
                        DESCR = (string)o["descr"];
                        insert.Bind(4, DESCR);
                        PRICE = (string)o["Price"];
                        insert.Bind(5, PRICE);
                        DAY = (string)o["day"];
                        insert.Bind(6, DAY);
                        insert.Step();
                    }
                }
            }
            catch (Exception)
            {
                if (count == 0)
                {
                    MessageBox.Show("Please check your internet connection");
                }
            }
            count++;
        }

        //Navigate to Tatva or Moksh or Lakshya section according to index of the element clicked in the list.
        private void TMLEventClick(object sender, SelectionChangedEventArgs e)
        {

            if (AboutList.SelectedItem == null)
                return;

            int selectedIndex = App.ViewModel.Items.IndexOf(AboutList.SelectedItem as ItemViewModel);

                switch(selectedIndex)
                {
                    case 0:
                        NavigationService.Navigate(new Uri("/DescriptionTatva.xaml", UriKind.Relative));
                    break;

                    case 1:
                        NavigationService.Navigate(new Uri("/DescriptionMoksh.xaml", UriKind.Relative));
                    break;

                    case 2:
                        NavigationService.Navigate(new Uri("/DescriptionLakshya.xaml", UriKind.Relative));
                    break;

                    default:

                    break;
                }

            AboutList.SelectedItem = null;
        }

        //Navigation for Registration click.
        private void Registration_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/RegistrationPage.xaml", UriKind.Relative));
        }
    }
}