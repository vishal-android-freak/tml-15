﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace TatvaMokshaLakshya
{
    public partial class MokshEvents : PhoneApplicationPage
    {
        long LastID;
        public string tml;
        string day;
        public MokshEvents()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var db = App.TMLEvents;

            //Check if data exists on first navigation to this page, in the table. 
            //if not, means there is no internet connection.
            try
            {
                using (var last = db.Prepare("SELECT eid FROM events_table ORDER BY eid DESC"))
                {
                    last.Step();
                    LastID = (Int64)last[0];
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please check your internet connection!");
            }


            //Add child elements to the main event list

            List<TMLEventsDay1> list = new List<TMLEventsDay1>();
            List<TMLEventsDay2> list1 = new List<TMLEventsDay2>();
            int i = 1;


            while (i <= LastID)
            {
                using (var tatva = db.Prepare("SELECT eid, event_name, tml, descr, Price FROM events_table WHERE eid=?"))
                {
                    tatva.Bind(1, i);
                    tatva.Step();

                    using (var version = db.Prepare("SELECT eid, tml FROM events_table WHERE eid=?"))
                    {
                        version.Bind(1, i);
                        version.Step();
                        tml = (string)version[1];
                    }

                    if (tml == "2")
                    {
                        using (var Day = db.Prepare("SELECT eid, day FROM events_table WHERE eid=?"))
                        {
                            Day.Bind(1, i);
                            Day.Step();
                            day = (string)Day[1];
                        }

                        if (day == "1")
                        {
                            list.Add(new TMLEventsDay1() { Event_Name = (string)tatva[1], Description = (string)tatva[3], Price = (string)tatva[4] });
                            EventsViewDay1.ItemsSource = list;
                        }
                        else if (day == "2")
                        {
                            list1.Add(new TMLEventsDay2() { Event_Name = (string)tatva[1], Description = (string)tatva[3], Price = (string)tatva[4] });
                            EventsViewDay2.ItemsSource = list1;
                        }
                        else if(day == "12")
                        {
                            list.Add(new TMLEventsDay1() { Event_Name = (string)tatva[1], Description = (string)tatva[3], Price = (string)tatva[4] });
                            EventsViewDay1.ItemsSource = list;

                            list1.Add(new TMLEventsDay2() { Event_Name = (string)tatva[1], Description = (string)tatva[3], Price = (string)tatva[4] });
                            EventsViewDay2.ItemsSource = list1;
                        }
                    }
                    i++;
                }
            }

        }
        public class TMLEventsDay1
        {
            public string Event_Name { get; set; }
            public string TML { get; set; }
            public string Description { get; set; }
            public string Price { get; set; }
        }

        public class TMLEventsDay2
        {
            public string Event_Name { get; set; }
            public string TML { get; set; }
            public string Description { get; set; }
            public string Price { get; set; }
        }

        private void MokshDay1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TMLEventsDay1 id = EventsViewDay1.SelectedItem as TMLEventsDay1;

            var _event_name = id.Event_Name;
            var _descr = id.Description;
            var _price = id.Price;

            NavigationService.Navigate(new Uri("/All.xaml?eName=" + _event_name + "&eDescr=" + _descr + "&ePrice=" + _price, UriKind.Relative));
        }

        private void MokshDay2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TMLEventsDay2 id = EventsViewDay2.SelectedItem as TMLEventsDay2;

            var _event_name = id.Event_Name;
            var _descr = id.Description;
            var _price = id.Price;

            NavigationService.Navigate(new Uri("/All.xaml?eName=" + _event_name + "&eDescr=" + _descr + "&ePrice=" + _price, UriKind.Relative));
        }
    }
}