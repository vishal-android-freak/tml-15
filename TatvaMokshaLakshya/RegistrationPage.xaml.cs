﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Threading;
using TatvaMokshaLakshya.ViewModels;

namespace TatvaMokshaLakshya
{
    public  partial class RegistrationPage : PhoneApplicationPage
    {
        
        public RegistrationPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);


            ////Add child elements to the main event list

            List<ListFull> list = new List<ListFull>();

            list.Add(new ListFull() { College = "Select College" });
            list.Add(new ListFull() { College = "A.C.Patil College of Engineering" });
            list.Add(new ListFull() { College = "Bharati Vidyapeeth College of Engineering" });
            list.Add(new ListFull() { College = "Datta Meghe College of Engineering" });
            list.Add(new ListFull() { College = "Don Bosco Institute of Technology" });
            list.Add(new ListFull() { College = "Fr. Conceicao Rodrigues Institute of Technology" });
            list.Add(new ListFull() { College = "Indira Gandhi College of Engineering" });
            list.Add(new ListFull() { College = "KC College of Engineering" });
            list.Add(new ListFull() { College = "K.J. Somaiya College of Engineering" });
            list.Add(new ListFull() { College = "Lokmanya Tilak College of Engineering" });
            list.Add(new ListFull() { College = "Pillais Institute of Information Technology" });
            list.Add(new ListFull() { College = "Ramrao Adik Institute of Technology" });
            list.Add(new ListFull() { College = "Saraswati College of Engineering" });
            list.Add(new ListFull() { College = "Shah and Anchor Kutchhi Engineering College" });
            list.Add(new ListFull() { College = "SIES Graduate School of Technology" });
            list.Add(new ListFull() { College = "Terna Engineering College" });
            list.Add(new ListFull() { College = "Thadomal Shahani Engineering College" });
            list.Add(new ListFull() { College = "Vidyalankar Institute of Technology" });
            list.Add(new ListFull() { College = "Veermata Jijabai Technological Institute" });
            list.Add(new ListFull() { College = "VESIT" });
            list.Add(new ListFull() { College = "Xavier Institute of Engineering" });
            list.Add(new ListFull() { College = "Other" });

            if (CollegeList.SelectedItem == null)
            {
                CollegeList.ItemsSource = list;
            }

            List<ListFull> Branchlist = new List<ListFull>();

            Branchlist.Add(new ListFull() { Branch = "Select Branch" });
            Branchlist.Add(new ListFull() { Branch = "Computer Engineering" });
            Branchlist.Add(new ListFull() { Branch = "Electronics & Telecom" });
            Branchlist.Add(new ListFull() { Branch = "Information Technology" });
            Branchlist.Add(new ListFull() { Branch = "Mechanical Engineering" });
            Branchlist.Add(new ListFull() { Branch = "Bio-technology Engineering" });
            Branchlist.Add(new ListFull() { Branch = "Automobile Engineering" });
            Branchlist.Add(new ListFull() { Branch = "Biomedical Engineering" });
            Branchlist.Add(new ListFull() { Branch = "Production Engineering" });
            Branchlist.Add(new ListFull() { Branch = "Chemical Engineering" });
            Branchlist.Add(new ListFull() { Branch = "Civil Engineering" });
            Branchlist.Add(new ListFull() { Branch = "Electrical Engineering" });
            Branchlist.Add(new ListFull() { Branch = "Electronics Engineering" });
            Branchlist.Add(new ListFull() { Branch = "Instrumentation" });

            if (BranchList.SelectedItem == null)
            {
                BranchList.ItemsSource = Branchlist;
            }
        }

        //Switch event list according to radio button selection
        private void Tatva_Checked(object sender, RoutedEventArgs e)
        {
            List<ListFull> list = new List<ListFull>();

            list.Add(new ListFull() { mainEvent = "Select an event" });
            list.Add(new ListFull() { mainEvent = "National Robotics Competiton" });
            list.Add(new ListFull() { mainEvent = "Robo Soccer" });
            list.Add(new ListFull() { mainEvent = "Robo F1" });
            list.Add(new ListFull() { mainEvent = "Robo Maze" });
            list.Add(new ListFull() { mainEvent = "Robo Hurdle" });
            list.Add(new ListFull() { mainEvent = "Line Follower" });
            list.Add(new ListFull() { mainEvent = "AquaBot" });
            list.Add(new ListFull() { mainEvent = "Entraption" });
            list.Add(new ListFull() { mainEvent = "Pick-n-Place" });
            list.Add(new ListFull() { mainEvent = "Buzz" });

            EventList.ItemsSource = list;
        }

        private void Moksh_Checked(object sender, RoutedEventArgs e)
        {
            List<ListFull> list = new List<ListFull>();

            list.Add(new ListFull() { mainEvent = "Select an event" });
            list.Add(new ListFull() { mainEvent = "Solo Singing" });
            list.Add(new ListFull() { mainEvent = "Duet Singing" });
            list.Add(new ListFull() { mainEvent = "Amrock" });
            list.Add(new ListFull() { mainEvent = "Karaoke" });
            list.Add(new ListFull() { mainEvent = "Bathroom to Limelight" });
            list.Add(new ListFull() { mainEvent = "Instrumedley" });
            list.Add(new ListFull() { mainEvent = "Solo Dance" });
            list.Add(new ListFull() { mainEvent = "Duet Dance" });
            list.Add(new ListFull() { mainEvent = "Bboying" });
            list.Add(new ListFull() { mainEvent = "Group Dance:Beatstruck" });
            list.Add(new ListFull() { mainEvent = "Big Boss" });
            list.Add(new ListFull() { mainEvent = "Street Play: Genesis" });
            list.Add(new ListFull() { mainEvent = "Film Festival :Director's Cut" });
            list.Add(new ListFull() { mainEvent = "Mono Acting" });
            list.Add(new ListFull() { mainEvent = "Face Painting" });
            list.Add(new ListFull() { mainEvent = "Teeshirt Painting" });
            list.Add(new ListFull() { mainEvent = "Painting without Brush" });
            list.Add(new ListFull() { mainEvent = "Caption action" });
            list.Add(new ListFull() { mainEvent = "Rangoli" });
            list.Add(new ListFull() { mainEvent = "Snap" });
            list.Add(new ListFull() { mainEvent = "Graffiti" });
            list.Add(new ListFull() { mainEvent = "Wrap up the Scrap" });
            list.Add(new ListFull() { mainEvent = "Just a Minute" });
            list.Add(new ListFull() { mainEvent = "Look! who is the cook!" });
            list.Add(new ListFull() { mainEvent = "HP Mania" });
            list.Add(new ListFull() { mainEvent = "Quiz" });
            list.Add(new ListFull() { mainEvent = "Logonix" });
            list.Add(new ListFull() { mainEvent = "Treasure Hunt" });
            list.Add(new ListFull() { mainEvent = "Mr and Ms Final" });
            list.Add(new ListFull() { mainEvent = "Snakes and ladders" });
            list.Add(new ListFull() { mainEvent = "Bowling" });
            list.Add(new ListFull() { mainEvent = "Temple Run" });
            list.Add(new ListFull() { mainEvent = "Pictionary" });
            list.Add(new ListFull() { mainEvent = "Hangman" });
            list.Add(new ListFull() { mainEvent = "Google Whacking" });
            list.Add(new ListFull() { mainEvent = "Creative Writing" });
            list.Add(new ListFull() { mainEvent = "Creative Speaking" });
            list.Add(new ListFull() { mainEvent = "Model UN" });

            EventList.ItemsSource = list;
        }

        private void Lakshya_Checked(object sender, RoutedEventArgs e)
        {
            List<ListFull> list = new List<ListFull>();

            list.Add(new ListFull() { mainEvent = "Select an event" });
            list.Add(new ListFull() { mainEvent = "Season Cricket" });
            list.Add(new ListFull() { mainEvent = "Open Football" });
            list.Add(new ListFull() { mainEvent = "Badminton" });
            list.Add(new ListFull() { mainEvent = "Table Tennis" });
            list.Add(new ListFull() { mainEvent = "Chess" });
            list.Add(new ListFull() { mainEvent = "Carrom" });
            list.Add(new ListFull() { mainEvent = "Volleyball" });
            list.Add(new ListFull() { mainEvent = "Basketball" });
            list.Add(new ListFull() { mainEvent = "Cage Football" });
            list.Add(new ListFull() { mainEvent = "Tug of War" });
            list.Add(new ListFull() { mainEvent = "Throw Ball" });

            EventList.ItemsSource = list;
        }

        public class ListFull
        {
            public string mainEvent { get; set; }
            public string College { get; set; }
            public string Branch { get; set; }
        }

        //Handle validation when registration button is clicked.
        private async void Register_Click(object sender, RoutedEventArgs e)
        {

            string email = Email.Text;
            string mobile = Mobile.Text;

            Regex regex = new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
+ "@"
+ @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");

            Match match = regex.Match(email);
            string result;
            if (!match.Success)
            {
                MessageBox.Show("Please enter correct email address!");
            }

            else if (mobile.Length < 10)
            {
                MessageBox.Show("Please enter correct mobile number!");
            }

            else if (Year.SelectedIndex == 0)
            {
                MessageBox.Show("Please select correct year!");
            }
            else if (CollegeList.SelectedIndex == 0)
            {
                MessageBox.Show("Please select correct college!");
            }
            else if (BranchList.SelectedIndex == 0)
            {
                MessageBox.Show("Please select correct branch!");
            }
            else if (EventList.SelectedIndex == -1 | EventList.SelectedIndex == 0)
            {
                MessageBox.Show("Please select correct event!");
            }
            else
            {
                try
                {
                    if (CollegeList.SelectedIndex == 21)
                    {
                        result = await RegisterData.PostRegisterData(FullName.Text, Email.Text, Mobile.Text, Year.SelectedIndex, BranchList.SelectedIndex, EventList.SelectedIndex, OtherBox.Text);
                    }
                    else
                    {
                        result = await RegisterData.PostRegisterData(FullName.Text, Email.Text, Mobile.Text, Year.SelectedIndex, BranchList.SelectedIndex, EventList.SelectedIndex, CollegeList.SelectedIndex.ToString());
                    }



                    MessageBox.Show(result);
                }
                catch (System.Net.WebException)
                {
                    MessageBox.Show("Please check your internet connection");
                }
            }
        }

        //Make textbox visible when other college is selected.
        private void CollegeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int otherIndex = CollegeList.SelectedIndex;

            if (otherIndex == 21)
            {
                OtherBox.Visibility = System.Windows.Visibility.Visible;
            }
        }
    }
}