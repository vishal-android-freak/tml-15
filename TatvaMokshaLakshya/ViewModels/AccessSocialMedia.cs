﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Phone.Tasks;

namespace TatvaMokshaLakshya.ViewModels
{
    class AccessSocialMedia
    {
        public void onLinkRedirect(string link)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();

            webBrowserTask.Uri = new Uri(link, UriKind.Absolute);
            webBrowserTask.Show();
        }
    }
}
