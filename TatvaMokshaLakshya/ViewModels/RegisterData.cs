﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace TatvaMokshaLakshya.ViewModels
{
    class RegisterData
    {
        public static async Task<string> PostRegisterData(string FullName, string Email, string Mobile, int Year, int Branch, int Event, string College)
        {
            HttpWebRequest request = HttpWebRequest.Create("http://www.tatvamoksh.siesgst.ac.in/register/test.php") as HttpWebRequest;

            // Set the Method property of the request to POST.
            request.Method = "POST";

            // Create POST data and convert it to a byte array.
            //StringBuilder postData = new StringBuilder("username=");
            //postData.Insert(9, FullName.Text);


            string postData = "username=" + FullName + "&email=" + Email + "&mobile=" + Mobile + "&year=" + Year + "&branch=" + Branch + "&college=" + College + "&event=" + Event;

            byte[] byteArray = Encoding.UTF8.GetBytes(postData.ToString());

            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            //Write data into a stream
            using (var stream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
            {
                stream.Write(byteArray, 0, byteArray.Length);
            }

            // Pick up the response:
            string result = null;
            using (var response = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null)))
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                result = reader.ReadToEnd();
            }

            return result;
        }


        public static async Task<string> GetEventDetails()
        {
            HttpWebRequest getevents = HttpWebRequest.Create("http://www.lux.site90.com/tml15.php") as HttpWebRequest;

            getevents.Method = "POST";

            string postData = "";

            byte[] byteArray = Encoding.UTF8.GetBytes(postData.ToString());

            // Set the ContentType property of the WebRequest.
            getevents.ContentType = "application/x-www-form-urlencoded";

            //Write data into a stream
            using (var stream = await Task.Factory.FromAsync<Stream>(getevents.BeginGetRequestStream, getevents.EndGetRequestStream, null))
            {
                stream.Write(byteArray, 0, byteArray.Length);
            }

            // Pick up the response:
            string finaloutput = null;
            using (var response = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(getevents.BeginGetResponse, getevents.EndGetResponse, null)))
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                finaloutput = reader.ReadToEnd();
            }

            return finaloutput;
        }
    }
}
